package vieon.training.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import vieon.training.databinding.FragmentBriefcaseBinding
import kotlin.random.Random


class BriefcaseFragment : Fragment() {

    private var _binding: FragmentBriefcaseBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBriefcaseBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray()

        binding.btnClickMe.setOnClickListener {

            binding.tvRandom.text = ""
            var randomStr: String = "AN"
            //Lấy số kí tự random của số
            val totalRandomDigit: Int = Random.nextInt(1, 5)
            //Lấy số kí tự random của chữ
            val totalRandomChar: Int = 6 - totalRandomDigit

            var sixDigit =
                str.filter { it.isDigit() }.shuffled()
                    .take(totalRandomDigit) + str.filter { it.isLetter() }.shuffled()
                    .take(totalRandomChar)
            sixDigit = sixDigit.shuffled()

            randomStr += sixDigit.joinToString("")

            binding.tvRandom.text = randomStr
        }


    }


}