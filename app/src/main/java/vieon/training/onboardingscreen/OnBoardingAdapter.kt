import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import vieon.training.R
import vieon.training.databinding.ItemOnBoardingBinding
import vieon.training.model.OnBoardingModel
import vieon.training.onboardingscreen.IOnNextButtonClick


class OnBoardingAdapter(
    private val buttonClickListener: IOnNextButtonClick,
    private val items: List<OnBoardingModel>
) :
    RecyclerView.Adapter<OnBoardingAdapter.ViewHolder>() {


    inner class ViewHolder(val binding: ItemOnBoardingBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemOnBoardingBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        with(holder) {
            with(items[position]) {
                binding.imageView.setImageResource(this.image)
                val imageParams = binding.imageView.layoutParams as ConstraintLayout.LayoutParams

                when (position) {
                    0 -> {
                        imageParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID
                    }

                    1 -> {
                        imageParams.width = ConstraintLayout.LayoutParams.MATCH_PARENT
                        imageParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID
                        binding.imageView.scaleType = ImageView.ScaleType.FIT_XY
                    }

                    else -> {
                        imageParams.endToEnd = ConstraintLayout.LayoutParams.UNSET
                        imageParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID


                        binding.buttonNext.text =
                            binding.root.context.getString(R.string.get_started)


                    }
                }

                binding.textTitle.text = binding.root.context.getString(this.textTitle)
                binding.textDescription.text = binding.root.context.getString(this.textDescription)
                binding.buttonNext.setOnClickListener {
                    buttonClickListener.onNextButtonClick(position)
                }


            }


        }
    }


    override fun getItemCount(): Int = items.size


}



