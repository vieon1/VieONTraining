package vieon.training.customview


import android.content.Context
import android.text.method.PasswordTransformationMethod
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import vieon.training.R.styleable
import vieon.training.R.styleable.CustomInputPassword
import vieon.training.databinding.InputPasswordCustomBinding


class InputPasswordCustom @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {


    private var showPass: Boolean = false
    private var binding: InputPasswordCustomBinding
    private var textTitle: String = ""
    private var textHint: String = ""


    init {

        binding = InputPasswordCustomBinding.inflate(LayoutInflater.from(context), this, true)

        binding.toggle.setOnClickListener {
            showPass = !showPass
            update(showPass)
        }
        val typedArray = context.obtainStyledAttributes(attrs, CustomInputPassword)

        textTitle = typedArray.getString(styleable.CustomInputPassword_text_title) ?: ""
        textHint = typedArray.getString(styleable.CustomInputPassword_text_hint) ?: ""
        binding.textHeader.text = textTitle
        binding.edt.hint = textHint

        typedArray.recycle()



        binding.edt.transformationMethod = AsteriskPasswordTransformationMethod()

    }

    private fun update(showPass: Boolean) {
        if (showPass) {
            binding.edt.transformationMethod = null
            binding.toggle.setImageResource(vieon.training.R.drawable.eye_open);
        } else {
            binding.edt.transformationMethod = AsteriskPasswordTransformationMethod()
            binding.toggle.setImageResource(vieon.training.R.drawable.eye_close);
        }
        // Move cursor to the end of the text
        binding.edt.text?.let { binding.edt.setSelection(it.length) }
    }


}

private class AsteriskPasswordTransformationMethod : PasswordTransformationMethod() {

    override fun getTransformation(source: CharSequence, view: View): CharSequence {
        return PasswordCharSequence(source)
    }

    private class PasswordCharSequence(private val source: CharSequence) : CharSequence {

        override val length: Int
            get() = source.length

        override fun get(index: Int): Char {
            return '*'
        }

        override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
            return PasswordCharSequence(source.subSequence(startIndex, endIndex))
        }
    }
}
